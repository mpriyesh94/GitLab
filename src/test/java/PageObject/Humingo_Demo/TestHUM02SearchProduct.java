package PageObject.Humingo_Demo;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import PageObject.Support.ExtentReport;
import PageObject.Support.HumingoLib;
import PageObject.Support.Log;

public class TestHUM02SearchProduct extends AddToShortlist {
	@Test
	public void testSearchProduct() {
	
		testCaseID = "HUM-2";
	//	summary=testCaseID +":Search a product from top search box";
		Log.startTestCase(testCaseID, "TestSearchProduct");
		
		try {
			message = incrementSteps()+" Launch Browser";
			launchDriver();
			Log.getReport(message);

			message = incrementSteps()+" Laumch URL";
			getURL();
			Log.getReport(message);
			
			message = incrementSteps()+" Initialize Element ";
			initializeElement();
			Log.getReport(message);
			
			message = incrementSteps()+" Login To Humingo";
			loginToHumingo(login_Email, login_Password);
			Log.getReport(message);
			
			message = incrementSteps()+" Search Product Using Search Box";
			filterProductBySearchTextBox(humingoObject);
			Log.getReport(message);

			message = incrementSteps()+" Verify Expected Search Result";
			verifyExpectedResult("Backpack");
			test.log(LogStatus.PASS,message);

			message = incrementSteps()+" Log Out Application";
			logout();
			Log.getReport(message);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.endTestCase("TestSearchProduct");
	}
