package PageObject.Humingo_Demo;

import org.testng.annotations.Test;
import PageObject.Support.Log;
import PageObject.Support.WebActions;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.InetAddress;

public class TestHUM01VerifyLogin extends WebActions{
	@Test
	public void testVerifyLogin() {
		Log.startTestCase(" 1 ", "TestVerifyLogin");
try {
    
         InetAddress addr;
	    addr = InetAddress.getLocalHost();
	    String hostname = addr.getHostName();
	    System.out.println(hostname);
	
		message = incrementSteps()+" Launch Browser";
		launchDriver();
		Log.getReport(message);
		
		message = incrementSteps()+" Launch URL";
		getURL();
		Log.getReport(message);
		
		message = incrementSteps()+" Initialize Element ";
		initializeElement();
		Log.getReport(message);
		
		message = incrementSteps()+" Login To Humingo ";
		loginToHumingo(login_Email,login_Password);
		Log.getReport(message);
		
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	Log.endTestCase("TestVerifyLogin");
}
	
}
